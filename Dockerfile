FROM condaforge/miniforge3:24.7.1-0

LABEL maintainer="cschu1981@gmail.com"
LABEL version="4a"
LABEL description="This is a Docker Image for motus4a"


ARG DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt upgrade -y

RUN apt install -y wget python3-pip git dirmngr gnupg ca-certificates build-essential libssl-dev libcurl4-gnutls-dev libxml2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev bwa samtools
RUN apt clean

RUN mkdir -p /opt/software/ && cd /opt/software/ && \
    wget https://sunagawalab.ethz.ch/share/MOTUS/motus-tool/v4.0.0a/mOTUsv4.0.0a.tar.gz && \
    tar xvf mOTUsv4.0.0a.tar.gz && \
    pip install pysam biopython && \
    sed -i 's/^\(DEFAULT_MOTUS_MGDB_LOCATION\) = \(.\+\)/\1 = pathlib.Path(os.environ.get("MOTUS_DB", str(\2)))/' motus-tool/motus.py && \
    sed -i '2303i\    print(f"{DEFAULT_MOTUS_MGDB_LOCATION=}")' motus-tool/motus.py && \
    sed -i '702 s:r\[0\]:r[0].replace("/1",""):; 703 s:r\[0\]:r[0].replace("/2",""):' motus-tool/motus.py && \
    ln -s motus.py motus-tool/motus

ENV PATH=/opt/software/motus-tool:$PATH
